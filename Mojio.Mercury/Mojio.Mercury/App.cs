﻿using Mojio.Mercury.ViewModels;
using Mojio.Mercury.Views;
using Xamarin.Forms;

namespace Mojio.Mercury
{
    public class App
    {
        public static void NavigateToVehicle(Vehicle vehicle)
        {
            _mojioApp.SelectedVehicle = vehicle;
            if (vehicle != null)
            {
                root.Navigation.PushAsync(tabbed);
            }
        }

        private static NavigationPage root = null;
        private static TabbedPage tabbed = null;
        private static VehiclesView vehicles = null;

        private static MojioApp _mojioApp = MojioApp.Current;
        public static Page GetMainPage()
        {
            _mojioApp.InitializationComplete = false;
            _mojioApp.Initialized = false;
            _mojioApp.Initialize().ContinueWith(t =>
            {
                _mojioApp.InitializationComplete = true;
                _mojioApp.Initialized = t.Result;
                _mojioApp.OnLoginSuccess += _mojioApp_OnLoginSuccess;
            });
            root = new NavigationPage(new LoginView());

            vehicles = new VehiclesView();
            tabbed = new TabbedPage()
            {
                Children =
                {
                    new LocateMyVehicle(),
                    new TripHistory(),
                }
            };
            return root;

        
        }

        static void _mojioApp_OnLoginSuccess()
        {
            root.Navigation.PushAsync(vehicles);
        }
    }
}
