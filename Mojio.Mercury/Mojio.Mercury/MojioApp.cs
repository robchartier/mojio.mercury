﻿using System;
using System.Net;
using System.Threading.Tasks;
using Mojio.Client;

namespace Mojio.Mercury
{
    public class MojioApp
    {
        private static MojioApp _current = null;
        private static object _lock = new object();

        public static MojioApp Current
        {
            get
            {
                if (_current != null) return _current;
                lock (_lock)
                {
                    _current = new MojioApp();
                }
                return _current;
            }
        }

        public bool Initialized { get; set; }
        public bool InitializationComplete { get; set; }
        private Guid appID = new Guid("9fef025f-966b-4ab7-b048-09cbca6a5faf");
        private Guid secretKey = new Guid("d93c5a5f-01ab-4aff-b2f2-19e59379d166");
        private MojioClient _client = null;

        public MojioClient Client
        {
            get { return _client; }
        }

        public Vehicle SelectedVehicle { get; set; }
        public MojioApp()
        {
            _client = new MojioClient(MojioClient.Live);
        }

        public async Task<bool> Initialize()
        {
            return await _client.BeginAsync(appID, secretKey);
        }

        public async Task<MojioResponse<Results<Vehicle>>> Vehicles()
        {
            if (UserToken != null && UserToken.Data !=null && UserToken.Data.UserId.HasValue)
            {
                return await _client.UserVehiclesAsync(UserToken.Data.UserId.Value);
            }
            return null;
        }

        public async Task<MojioResponse<Token>> Login(string email, string password)
        {
            var login = await _client.SetUserAsync(email, password);
            UserToken = login;
            if (OnLoginSuccess != null && login.StatusCode == HttpStatusCode.OK)
            {
                OnLoginSuccess();
            }
            return login;
        }
        private MojioResponse<Token> UserToken { get; set; }
        public delegate void LoginSuccess();
        public event LoginSuccess OnLoginSuccess;
    }
}