﻿using System;
using System.Windows.Input;
using Mojio.Mercury.ViewModels;

namespace Mojio.Mercury.Commands
{
    public abstract class MojioCommand : ICommand
    {
        protected MojioApp MojioApp;
        protected MojioViewModel ViewModel;

        public MojioCommand(MojioApp client, MojioViewModel viewModel)
        {
            MojioApp = client;
            ViewModel = viewModel;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public abstract void Execute(object parameter);

        public event EventHandler CanExecuteChanged;

    }
}
