﻿using System.Net;
using Mojio.Client;
using Mojio.Mercury.ViewModels;

namespace Mojio.Mercury.Commands
{
    public delegate void LoginComplete(MojioResponse<Token> response);

    public class LoginCommand : MojioCommand
    {
        public event LoginComplete OnLoginComplete;

        public LoginCommand(MojioApp client, MojioViewModel viewModel) : base(client, viewModel)
        {
        }

        public override void Execute(object parameter)
        {
            var vm = base.ViewModel as LoginViewModel;
            if (vm !=null && OnLoginComplete != null)
            {
                vm.StatusText = "Attemping to login, please wait...";
                MojioApp.Login(vm.Email, vm.Password).ContinueWith(t =>
                {
                    if (t.IsCompleted && t.Result.StatusCode == HttpStatusCode.OK && t.Result.Data.UserId.HasValue)
                    {
                        vm.StatusText = "Login was a success.";
                    }
                    else
                    {
                        vm.StatusText = "Login failed, try again.";
                    }
                    
                    OnLoginComplete(t.Result);
                });
            }
        }
    }
}