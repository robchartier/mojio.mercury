﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Mojio.Client.Linq;
using Mojio.Mercury.Commands;

namespace Mojio.Mercury.ViewModels
{
    public class VehiclesViewModel : MojioViewModel
    {
        private List<Vehicle> _vehicles;

        public VehiclesViewModel()
        {
            Title = "My Vehicles";
            MojioApp.Current.OnLoginSuccess += OnLoginSuccess;
        }

        private Vehicle _selectedVehicle;
        public Vehicle SelectedVehicle
        {
            get { return _selectedVehicle; }
            set
            {
                _selectedVehicle = value; 
                OnPropertyChanged();
                App.NavigateToVehicle(_selectedVehicle);
            }
        }

        private void OnLoginSuccess()
        {
            MojioApp.Current.Vehicles().ContinueWith(t =>
            {
                var lst = new List<Vehicle>();
                lst.AddRange(t.Result.Data.Data);
                Vehicles = lst;
            });
        }
        public List<Vehicle> Vehicles
        {
            get { return _vehicles; }
            set { _vehicles = value; OnPropertyChanged(); }
        }

    }
}
