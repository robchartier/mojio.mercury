﻿using System.Windows.Input;
using Mojio.Mercury.Commands;

namespace Mojio.Mercury.ViewModels
{
    public class LoginViewModel : MojioViewModel
    {
        public LoginViewModel()
        {
            EmailText = "Email:";
            PasswordText = "Password:";
            LoginButtonText = "Login";
            StatusText = "Login using your Moj.io Credentials";
            Title = "Login to Moj.io!";
        }


        private string _email;
        private string _password;

        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                OnPropertyChanged();
            }
        }

        private string _emailText;
        private string _passwordText;

        public string EmailText
        {
            get { return _emailText; }
            set
            {
                _emailText = value;
                OnPropertyChanged();
            }
        }

        public string PasswordText
        {
            get { return _passwordText; }
            set
            {
                _passwordText = value;
                OnPropertyChanged();
            }
        }

        public string StatusText
        {
            get { return _statusText; }
            set { _statusText = value; OnPropertyChanged(); }
        }


        private string _loginButtonText;

        public string LoginButtonText
        {
            get { return _loginButtonText; }
            set
            {
                _loginButtonText = value;
                OnPropertyChanged();
            }
        }

        private ICommand loginCommand;

        public ICommand LoginCommand
        {
            get
            {
                if (loginCommand == null)
                {
                    loginCommand = new LoginCommand(MojioApp.Current, this);
                    (loginCommand as LoginCommand).OnLoginComplete += LoginViewModel_OnLoginComplete;
                }
                return loginCommand;
            }
            set { loginCommand = value; }
        }

        private bool _loggedIn;
        public bool LoggedIn
        {
            get { return _loggedIn; }
            set { _loggedIn = value; OnPropertyChanged(); }
        }

        private Client.MojioResponse<Token> _response;
        private string _statusText;

        void LoginViewModel_OnLoginComplete(Client.MojioResponse<Token> response)
        {
            _response = response;
            
        }

    }
}
