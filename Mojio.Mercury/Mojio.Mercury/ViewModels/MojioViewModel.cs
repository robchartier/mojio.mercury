﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Mojio.Mercury.ViewModels
{
    public class MojioViewModel : INotifyPropertyChanged
    {


        private string _title;

        public string Title
        {
            get { return _title; }
            set { _title = value; OnPropertyChanged(); }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
