﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Maps;

namespace Mojio.Mercury.ViewModels
{
    public class LocateMyVehicleViewModel : MojioViewModel
    {
        private List<Pin> _pins;
        private Position _centerPosition;

        public LocateMyVehicleViewModel()
        {
            Title = "Locate my Vehicle";
            Pins = new List<Pin>();
        }

        public void Update()
        {
            var vehicle = MojioApp.Current.SelectedVehicle;
            if (vehicle != null)
            {
                var p = new List<Pin>();
                CenterPosition = new Position(vehicle.LastLocation.Lat, vehicle.LastLocation.Lng);
                p.Add(new Pin()
                {
                    Position = CenterPosition,
                    Label = "Last Location",
                });
                Pins = p;
            }
        }

        public Position CenterPosition
        {
            get { return _centerPosition; }
            set
            {
                _centerPosition = value;
                OnPropertyChanged();
            }
        }

        public List<Pin> Pins
        {
            get { return _pins; }
            set
            {
                _pins = value;
                
                OnPropertyChanged();
            }
        }
    }
}
