﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mojio.Mercury.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Mojio.Mercury.Views
{
    public partial class LocateMyVehicle : ContentPage
    {
        public LocateMyVehicle()
        {
            this.BindingContext = new LocateMyVehicleViewModel();
            (this.BindingContext as LocateMyVehicleViewModel).PropertyChanged += LocateMyVehicle_PropertyChanged;
            InitializeComponent();
        }

        void LocateMyVehicle_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var vm = (this.BindingContext as LocateMyVehicleViewModel);
            if (vm != null)
            {
                if (e.PropertyName == "Pins" && vm.Pins !=null && vm.Pins.Count>0)
                {
                    VehicleMap.Pins.Clear();
                    foreach (var p in vm.Pins)
                    {
                        VehicleMap.Pins.Add(p);                        
                    }
                }
                else if (e.PropertyName == "CenterPosition")
                {
                    VehicleMap.MoveToRegion(MapSpan.FromCenterAndRadius(vm.CenterPosition, Distance.FromMiles(0.5)));
                }
            }
        }

        protected override void OnAppearing()
        {
            var vm = (this.BindingContext as LocateMyVehicleViewModel);
            vm.Update();
            base.OnAppearing();
            VehicleMap.MoveToRegion(MapSpan.FromCenterAndRadius(vm.CenterPosition, Distance.FromMiles(0.5)));
        }
    }
}
