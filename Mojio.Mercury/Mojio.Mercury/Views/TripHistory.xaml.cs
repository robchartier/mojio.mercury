﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mojio.Mercury.ViewModels;
using Xamarin.Forms;

namespace Mojio.Mercury.Views
{
    public partial class TripHistory : ContentPage
    {
        public TripHistory()
        {
            this.BindingContext = new TripHistoryViewModel();
            InitializeComponent();
        }
    }
}
