﻿using Mojio.Mercury.ViewModels;
using Xamarin.Forms;

namespace Mojio.Mercury.Views
{
    public partial class LoginView : ContentPage
    {
        public LoginViewModel LoginViewModel { get; set; }
        public LoginView()
        {
            LoginViewModel = new LoginViewModel();
            this.BindingContext = LoginViewModel;
            InitializeComponent();
        }
    }
}
