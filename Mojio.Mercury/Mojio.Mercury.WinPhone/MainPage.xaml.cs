﻿using Microsoft.Phone.Controls;
using Xamarin.Forms;


namespace Mojio.Mercury.WinPhone
{
    public partial class MainPage : PhoneApplicationPage
    {
        public MainPage()
        {
            InitializeComponent();

            Forms.Init();
            Xamarin.FormsMaps.Init();
            Content = Mojio.Mercury.App.GetMainPage().ConvertPageToUIElement(this);
        }
    }
}
