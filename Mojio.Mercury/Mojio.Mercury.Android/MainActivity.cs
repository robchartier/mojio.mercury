﻿using Android.App;
using Android.OS;

using Xamarin.Forms.Platform.Android;

namespace Mojio.Mercury.Droid
{
    [Activity(Label = "Mojio.Mercury", MainLauncher = true)]
    public class MainActivity : AndroidActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            Xamarin.Forms.Forms.Init(this, bundle);
            Xamarin.FormsMaps.Init();
            SetPage(App.GetMainPage());
        }
    }
}

